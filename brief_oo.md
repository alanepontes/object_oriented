##Object-Oriented Programming

Aplicações OO são feitas de objetos e de mensagens passadas entre eles. 

##Procedural Languages

OO é relativa a programação não OO, ou procedural. É intuitivo pensar nas diferenças desses dois termos. 

Imagine uma linguagem de programação procedural qualquer, uma na qual você cria um simples script. Nessa linguagem você define variáveis, isto é, nomear e associar esses nomes a bits de dados. Uma vez atribuído, os dados associados podem ser acessados pela referência para a variável.

Como toda linguagem procedural, é conhecido um pequeno e fixado conjunto de diferentes tipos de dados, como Strings, numbers, arrays, files e por ai vai. Esses diferentes tipos de dados são conhecidos como data types.
Cada um deles descreve um tipo bastante específico. Uma String é diferente de um file, por exemplo. A sintaxe da linguagem contem operações built-in para fazer algumas coisas para os diferentes data types.
É possível concatenar Strings e ler arquivos, por exemplo.

Como você cria as variáveis, você sabe o tipo que elas guardam. Suas expectativas sobre quais operações você pode usar são baseadas no seu conhecimento dos data types das variáveis. Você sabe que você pode concatenar strings, fazer cálculos com números, indexar arrays, ler arquivos...

Data type e operação possível já existe; essas coisas são built-in (construídas) dentro da sintaxe da linguagem. A linguagem deve deixar você criar funções (grupos de operações predefinidas juntas dentro de um novo nome) ou definir estruturas de dados complexas mas você não pode realmente criar novas operações ou novos tipos de dados. O que vocễ vê é tudo que você tem.

Nessa linguagem imaginária, assim como toda linguagem procedural, há uma grande distância entre dados e comportamento. Dado é uma coisa, comportamento é outra completamente diferente. Dados ficam guardados dentro das variáveis e então são passados para os comportamentos, e, em geral, faz algo por ele.
Dados são como uma esposa que o comportamento a envia para o trabalho todo dia, não tem nenhum jeito de saber o que realmente acontece enquanto ela está  fora de vista. As influências sobre os dados pode ser imprevisível e em grande parte indetectável.


## Object-Oriented Languages

Agora imagine um tipo diferente de linguagens de programação, uma orientada a objetos e baseada em classes, como o Ruby. Ao invés de dividir dados e comportamento, Ruby os junta dentro de um 'objeto'. Objetos tem comportamento e podem conter dados, que eles controlam sozinhos.
Objetos usam comportamentos de outros, trocando mensagens entre si.

Ruby tem uma String objeto ao invés de uma String data type. As operações que funcionam com strings são built-in no objeto string ao invés de built-in na sintaxe da linguagem. Como objetos suportam as próprias operações, Ruby não precisa saber nada em particular sobre string data type; ele precisa apenas prover uma maneira geral para objetos enviar mensagens. Se uma string entende o a mensagem 'concat', Ruby não tem que conter uma sintaxe para concatenar strings, ele apenas tem que prover um jeito para um objeto enviar concat para outro.

Ruby permite você definir uma classe que provê um diagrama para a contrução de objetos similares. Uma classe define métodos(comportamento) e atributos(variáveis). Métodos se invocados respondem mensagens. O mesmo nome de método pode ser definido em objetos diferentes. Cabe ao Ruby encontrá-lo e invocar o método certo do objeto correto para qualquer envio de mensagem.

Uma vez que a classe String existe ela pode ser usada para criar novas instâncias de um objeto string. Todas as novas instancias implementam o mesmo método e tambem usam os mesmos atributos, mas cada uma tem os seus próprios dados pessoais. Elas compartilham os mesmos métodos então elas todas se comportam como strings; elas contem diferentes dados então elas representam objetos diferentes.

A class String define um tipo que é mais do que um simples dado. Conhecer um tipo de um objeto leva você a ter expectativa sobre como ele se comporta. Em linguagens procedurais, variáveis tem um único tipo de dado. Conhecer esses tipos de dados deixam você ter expectativa sobre quais operações são válidas. Em Ruby um objeto pode ter muitos tipos, um deles sempre irá vir da classe. Conhecer um tipo de um objeto, portanto, deixa você ter expectativa sobre quais mensagens ele responde.

Linguagens OO são construídas usando objetos e é aqui que as coisas começam a ficar interessante. 
A classe String, isso é, o diagrama para novos objetos strings, é ele mesmo um objeto. É uma instancia da classe Class. Assim como todo objeto string é uma instancia especifica de uma classe String, todo objeto class (String, Fixnum, ad infinitum) é um instancia especifica da classe Class. A class String fabrica novas strings, a classe Class fabrica novas classes.

Então, linguagens OO, são open-ended. Elas não limitam você a um conjunto de built-in types e operações pre-definidas. Você pode criar seus tipos. Cada aplicação OO gradualmente se torna uma única linguagem de programação que é especificamente adaptada ao seu domínio.



