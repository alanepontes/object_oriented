##Design Principles
---

###SOLID:
 * criado por Michal Feathers
 * popularizado por Robert Martin
 * representa 5 dos mais bem conhecidos princípios do design orientado a objetos
 * **S**ingle Resposibility
 * **O**pen-Closed
 * **L**iskov Substitution
 * **I**nterface Segregation
 * **D**ependency Inversion

###Outros:
 * **DRY** (Don't Repeat Yourself)
    - criado por Andy Hunt e Dave Thomas's
 * **Lod** (Law of Demeter)   



