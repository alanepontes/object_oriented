##As quatro regras da Sandi Metz para desenvolvedores

A [Sandi](http://www.sandimetz.com/)

>**As 4 regras:**  
1. Classes não podem ter mais do que 100 linhas de código  
2. Métodos não devem ter mais do que 5 linhas de código  
3. Não passe mais do que 4 parametros para um método. Opções Hash são parâmetros.  
4. Controllers podem instanciar apenas um objeto. View podem somente conhecer sobre uma variável de instância e view devem enviar mensagens apenas para esse objeto.  
